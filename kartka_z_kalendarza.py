# Importujemy potrzebne biblioteki
import csv
import time
import pandas as pd
from selenium import webdriver
from selenium.webdriver.common.by import By
import re
import telegram
import asyncio
from datetime import date

driver = webdriver.Chrome()


#tylko na dziś
stronakalendarz = "https://cerkiew.pl/kalendarz/"
driver.get(stronakalendarz)
textdata = driver.find_element(By.XPATH,"/html[1]/body[1]/content[1]/div[1]/div[1]/aside[1]/section[1]/div[2]/div[2]/span[1]").text
textdata = re.sub(r'\s+', ' ', textdata)


#Nadchodzące liturgie
stronaKrakowCerkiewData = "https://krakow-cerkiew.pl/"
driver.get(stronaKrakowCerkiewData)
textNadchodzaceLiturgie1data = driver.find_element(By.XPATH,    "/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/section[1]/ul[1]/li[1]/div[2]/span[1]").text
textNadchodzaceLiturgie1liturgia = driver.find_element(By.XPATH,"/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/section[1]/ul[1]/li[1]/div[2]/h4[1]/a[1]").text

textNadchodzaceLiturgie2data = driver.find_element(By.XPATH,    "/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/section[1]/ul[1]/li[2]/div[2]/span[1]").text
textNadchodzaceLiturgie2liturgia = driver.find_element(By.XPATH,"/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/section[1]/ul[1]/li[2]/div[2]/h4[1]/a[1]").text

textNadchodzaceLiturgie3data = driver.find_element(By.XPATH,    "/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/section[1]/ul[1]/li[3]/div[2]/span[1]").text
textNadchodzaceLiturgie3liturgia = driver.find_element(By.XPATH,"/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/section[1]/ul[1]/li[3]/div[2]/h4[1]/a[1]").text

textNadchodzaceLiturgie4data = driver.find_element(By.XPATH,    "/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/section[1]/ul[1]/li[4]/div[2]/span[1]").text
textNadchodzaceLiturgie4liturgia = driver.find_element(By.XPATH,"/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/section[1]/ul[1]/li[4]/div[2]/h4[1]/a[1]").text

textNadchodzaceLiturgie5data = driver.find_element(By.XPATH,    "/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/section[1]/ul[1]/li[5]/div[2]/span[1]").text
textNadchodzaceLiturgie5liturgia = driver.find_element(By.XPATH,"/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/section[1]/ul[1]/li[5]/div[2]/h4[1]/a[1]").text


nabozenstwo1 = textNadchodzaceLiturgie1data + " " + textNadchodzaceLiturgie1liturgia
nabozenstwo2 = textNadchodzaceLiturgie2data + " " + textNadchodzaceLiturgie2liturgia
nabozenstwo3 = textNadchodzaceLiturgie3data + " " + textNadchodzaceLiturgie3liturgia
nabozenstwo4 = textNadchodzaceLiturgie4data + " " + textNadchodzaceLiturgie4liturgia
nabozenstwo5 = textNadchodzaceLiturgie5data + " " + textNadchodzaceLiturgie5liturgia

today = date.today()
date_str = today.strftime("%Y-%m-%d")
stronakalendarzCzytania = f"https://cerkiew.pl/czytania/{date_str}"
stronakalendarzCzytaniaMessage = f'<a href="{stronakalendarzCzytania}"><b>Czytania dnia:</b></a>'

driver.get(stronakalendarz)

# Find all div elements in the section
div_elements = driver.find_elements(By.XPATH, '//section/div')

# Find the index of the div element that contains "CZYTANIA"
czt_index = None
for i, div_el in enumerate(div_elements):
    if "CZYTANIA" in div_el.text:
        czt_index = i
        break


next_div_xpath = f"//section/div[{czt_index+2}]"

czytaniaText = driver.find_element(By.XPATH, next_div_xpath).text

# Find the index of the div element that contains "ŚWIĘCI DNIA"
czt_index = None
for i, div_el in enumerate(div_elements):
    if "ŚWIĘCI DNIA" in div_el.text:
        czt_index = i
        break


next_div_xpath2 = f'//section/div[{czt_index+2}]'

swieciDniaText = driver.find_element(By.XPATH, next_div_xpath2).text

message2 = f"<b>Kartka z kalendarza</b> \n{textdata} \n\n<b>Nadchodzące nabożeństwa:</b>\n{nabozenstwo1}\n \n{nabozenstwo2}\n \n{nabozenstwo3}\n \n{nabozenstwo4}\n \n{nabozenstwo5} \n\n{stronakalendarzCzytaniaMessage}\n{czytaniaText}\n\n<b>Święci dnia:</b>\n{swieciDniaText}"
print(message2)


async def send_telegram_message(api_token, chat_id, message):
    bot = telegram.Bot(token=api_token)
    await bot.send_message(chat_id=chat_id, text=message, parse_mode='HTML')

async def main():
    api_token = 'XYZ'
    chat_id = "@test_kartkazkalendarza"
    message = message2

    await send_telegram_message(api_token, chat_id, message)

asyncio.run(main())




