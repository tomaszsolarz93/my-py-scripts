import pymongo
import pandas as pd
import logging
import os
import getpass
from datetime import datetime

# Configure logging
logging.basicConfig(filename="collection_stats.log", level=logging.INFO, format='%(asctime)s %(message)s')

# Prompt user for username and password
username = input("Enter MongoDB username: ")
password = getpass.getpass("Enter password: ")

# Construct connection string with user and password
conn_string = f"mongodb+srv://{username}:{password}@cluster0.kvs206r.mongodb.net/test"

# Connect to MongoDB
client = pymongo.MongoClient(conn_string)
db = client["sample_training"]

# Get the collection names and stats
collection_stats = []
for collection_name in db.list_collection_names():
    stats = db.command("collStats", collection_name)
    size_gb = stats["size"] / (1024**3)
    storage_size_gb = stats["storageSize"] / (1024**3)
    collection_stats.append({"Name": collection_name, "Size": size_gb, "Storage Size": storage_size_gb})


# Create a DataFrame from the stats
df = pd.DataFrame(collection_stats)

# Get the current date
export_date = datetime.now().strftime("%Y-%m-%d %H_%M_%S")

import logging

# Configure the logging module
logging.basicConfig(filename='errors.log', level=logging.ERROR)

try:
    # Your existing code here
    export_date = datetime.now().strftime("%Y-%m-%d %H_%M_%S")
    file_name = "collection_stats.xlsx"
    if not os.path.exists(file_name):
        with pd.ExcelWriter(file_name, engine="openpyxl") as writer:
            df.to_excel(writer, sheet_name=export_date.replace(":", "_"), index=False)
    else:
        try:
            with pd.ExcelWriter(file_name, engine="openpyxl", mode="a") as writer:
                df.to_excel(writer, sheet_name=export_date.replace(":", "_"), index=False)
        except PermissionError as e:
            logging.error(e)
            new_file_name = "collection_stats_" + datetime.now().strftime("%Y-%m-%d_%H-%M-%S") + ".xlsx"
            with pd.ExcelWriter(new_file_name, engine="openpyxl") as writer:
                df.to_excel(writer, sheet_name=export_date.replace(":", "_"), index=False)
except Exception as e:
    logging.error(e)
else:
    print("Success: Collection stats exported to collection_stats.xlsx ")

